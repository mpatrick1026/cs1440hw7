#include <iostream>
#include <map>
#include <regex>
#include <sstream>
#include <cstdlib>
#include <unordered_map>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

using namespace std;

const string REGEXSTRING="<a .*?href=\"((?:(http:\/\/)([^\/\"#]+)|\/)([^\"]*))\".*?>";
const regex ATAG(REGEXSTRING);  //This is an edited version of the A tag regex made by Hunter Henrichsen found at https://bitbucket.org/snippets/hhenrichsen/xeerEd

int nextURL(string url, unordered_map<string,int> &urlList, int depthLeft, int maxDepth){
    try{
        if(depthLeft==0)return 0;//stop once at max depth.
        
        curlpp::Easy request;
        curlpp::options::Url curlUrl(url);
        request.setOpt(curlUrl);
        curlpp::options::FollowLocation follow(true);
        request.setOpt(follow);
        ostringstream htmlCode;
        htmlCode << request << endl;
        string html(htmlCode.str());//put the html code into a string for easier use.
        curlpp::Cleanup clean;//make sure it gets cleaned up at the end
        // cout << html << endl;
        // cout.flush();
        
        //this is where we parse the html code
        smatch foundUrls;
        while(regex_search(html,foundUrls,ATAG)){
            string thisUrl=foundUrls[1];
            if(thisUrl[0]=='/'){//make sure we have the whole url before anything else.
                string temp=url;
                if(temp.back()=='\/'){
                    temp.pop_back();//if there is a \ at the end of the previous url, we want to delete it so we don't have a double \\.
                }
                temp+=thisUrl;
                thisUrl=temp;
            }
            if(urlList.count(thisUrl)==0){//if we can't find the url in the map, we need to go deeper.
                for(int i=depthLeft;i<maxDepth;i++){
                    cout<<"\t";
                }
                
                cout<<thisUrl<<endl;
                urlList[thisUrl]=1;
                nextURL(thisUrl, urlList, depthLeft-1, maxDepth);
            }
            //else{cout<<"we have a repeat"<<endl<<endl<<endl;}
            html=foundUrls.suffix().str();
         }
        return EXIT_SUCCESS;
    }
    catch ( curlpp::LogicError & e ) {
        std::cout << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
        std::cout << e.what() << std::endl;
    }

    return EXIT_FAILURE;
}






int main(int argc, char** argv) {
    string there = "http://cs.usu.edu";
    int maxDepth = 3;

    if (argc > 1)
        there = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);

    cout << "Recursively crawling the web to a depth of " << maxDepth << " links beginning from " << there << endl;
    unordered_map<string,int> urlList;
    nextURL(there,urlList,maxDepth, maxDepth);
    //  _____ ___  ___   ___  
    // |_   _/ _ \|   \ / _ (_)
    //   | || (_) | |) | (_) |
    //   |_| \___/|___/ \___(_)
    //                        
    // 0. Define a recursive function that will, given a URL and a maximum
    //    depth, follow all hyperlinks found until the maximum depth is
    //    reached. You may define a wrapper function to make calling this
    //    recursive function from main() easier.
    //
    // 1. Your recursive function will create a new curlpp::Easy object each
    //    time it's called.  it should also clean up after itself.
    //
    // 2. Your recursive function needs to account for relative URLs. For
    //    example, the address of Don Knuth's FAQ page is
    //    http://www-cs-faculty.stanford.edu/~knuth/faq.html. If you inspect
    //    the contents of the page with your browser's developer tools
    //    (Ctrl-Shift-I) or view the source (Ctrl-U), you'll see that many of
    //    the links therein do not begin with
    //    http://www-cs-faculty.stanford.edu/.
    //
    //    Your recursive function needs to account for this by remembering what
    //    the current domain name is, and being prepared to prepend that to the
    //    URL it parses out of any given hyperlink.
    //
    // 3. Your recursive function should skip hyperlinks beginning with # -
    //    they refer to locations on the same page.
    //
    // 4. Your recursive function must also keep track of all pages it's
    //    visited so that it doesn't waste time visiting the same one again and
    //    again. This is where the map comes in handy. Your recursive
    //    function takes it as an argument, and returns it, possibly modified,
    //    after each invocation.


    return 0;
}
